# Makefile for Blogging using GNU Emacs & Org mode

# Usage:
# `make` or `make publish`: Publish files using available Emacs configuration.
# `make publish_no_init`: Publish files without using Emacs configuration.
# `make clean`: Delete existing public/ directory and cached file under ~/.org-timestamps/

# Local testing:
# `python3 -m http.server --directory=public/`          <-- (The '--directory' flag is available from Python 3.7)

.PHONY: all publish publish_no_init

EMACS =

ifndef EMACS
EMACS = "emacs"
endif

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(dir $(abspath $(firstword $(MAKEFILE_LIST))))

all: publish

debug: publish.el
	@echo "DEBUG: Publishing... with current Emacs configurations."
	@env HOME=${current_dir}/tmp ${EMACS} --batch --eval "(debug-on-entry 'indent-region-line-by-line)" --load publish.el --funcall org-publish-all


publish: publish.el
	@echo "Publishing... with current Emacs configurations."
	@env HOME=${current_dir}/tmp ${EMACS} -batch -f batch-byte-compile publish.el
	@env HOME=${current_dir}/tmp ${EMACS} --batch --load publish.el --funcall org-publish-all

publish_no_init: publish.el
	@echo "Publishing... with --no-init."
	${EMACS} --batch --no-init --load publish.el --funcall org-publish-all

clean:
	@echo "Cleaning up.."
	@rm -rvf public
	@rm -rvf ${current_dir}/tmp/.org-timestamps/*

bootstrap:
	@echo "Cleaning up temporary .emacs.d folder.."
	@rm -rvf *.elc
	@rm -rvf ${current_dir}/tmp/.emacs.d/*

test:
	@echo "Starting Python module http.server for testing.."
	@python3 -m http.server --directory=public/

install-afs:
	@echo "Transfering files to AFS..."
	@rsync -rtlv --delete --exclude '*~' public/ /afs/hcoop.net/user/h/ha/hanno/public_html/

install:
	@echo "Transfering files via SSH w/ Kerberos auth..."
	@rsync -rtlv -e "ssh -oGSSAPIAuthentication=yes -oGSSAPIDelegateCredentials=yes " --delete --exclude '*~' public/ ssh.hcoop.net:public_html/
