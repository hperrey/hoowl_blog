;; publish.el --- Publish org-mode project on Gitlab Pages
;; Author: Hanno Perrey
;; Based on code by Sachin Patil

;;; Commentary:
;; This script will convert the org-mode files in this directory into
;; html.

;;; Code:

;; install and setup straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
      (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
        "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
        'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(straight-use-package 'use-package)
(setq straight-use-package-by-default t)
(straight-use-package 'org)

(use-package ox-rss
  :straight (ox-rss :host github
                    :repo "benedicthw/ox-rss"
                    :branch "master"))

(use-package htmlize)

;; setting to nil, avoids "Author: x" at the bottom
(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

(defvar psachin-date-format "%b %d, %Y")

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format psachin-date-format
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link t
      org-html-doctype "html5"
      org-html-htmlize-output-type 'css
      org-src-fontify-natively t)

;; use American locale to correctly print days and months in English
(setq system-time-locale "en_US.UTF-8")

(defvar hanno/tag-directory "tags")

(setq loomcom/project-dir (file-truename "./"))
(setq loomcom/header-file
      (concat loomcom/project-dir "page/header.html"))
(setq loomcom/preamble-file
      (concat loomcom/project-dir "page/preamble.html"))
(setq loomcom/footer-file
      (concat loomcom/project-dir "page/footer.html"))

(setq org-id-locations-file (concat loomcom/project-dir "tmp/" ".orgids"))
;; avoid creating backup files
(setq make-backup-files nil)

;; working around an error due to issues when using org-set-property and the new folding mechanism in Org versions >9.6
;; see https://adam.kruszewski.name/2022-05-08-org-publish-call-org-fold-core-region-error.html
(set-variable org-fold-core-style  'overlay)
;; working around errors due to org cache element being confused by our buffer editing
(setq org-element-use-cache nil)


;; helper routine (from lang/org/autoload/org.el which is part of DOOM Emacs)
(defun +org-get-global-property (name &optional file bound)
  "Get a document property named NAME (string) from an org FILE (defaults to
current file). Only scans first 2048 bytes of the document."
  (unless bound
    (setq bound 256))
  (if file
      (with-temp-buffer
        (insert-file-contents-literally file nil 0 bound)
        (+org--get-property name))
    (+org--get-property name bound)))

;; helper routine (from lang/org/autoload/org.el which is part of DOOM Emacs)
(defun +org--get-property (name &optional bound)
  (save-excursion
    (let ((re (format "^#\\+%s:[ \t]*\\([^\n]+\\)" (upcase name))))
      (goto-char (point-min))
      (when (re-search-forward re bound t)
        (buffer-substring-no-properties (match-beginning 1) (match-end 1))))))


(defvar psachin-website-html-head
  (with-temp-buffer
    (insert-file-contents loomcom/header-file)
    (buffer-string)))

(defun psachin-website-html-preamble (plist)
  "PLIST: An entry."
  ;; Skip adding subtitle to the post if :FILETAGS don't have 'post' as a
  ;; tag
  (when (string-match-p "post" (format "%s" (plist-get plist :filetags)))
    (plist-put plist
	       :subtitle (format "Published on %s."
                           (org-export-get-date plist psachin-date-format)
                           (plist-get plist :filetags)
                           ;;(car (plist-get plist :author))
                           )))
    ;; Below content will be added anyways from file
  (with-temp-buffer
    (insert-file-contents loomcom/preamble-file)
    (buffer-string)))

(defvar psachin-website-html-postamble
  (with-temp-buffer
    (insert-file-contents loomcom/footer-file)
    (buffer-string)))

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")


(defun hanno/org-publish-org-sitemap (title list)
  "Convert the sitemap from a LIST to a subtree using TITLE as title."
  (concat (format "#+TITLE: %s\n\n" title)
          (org-list-to-subtree list)))


(defun loomcom/get-preview (filename)
  "Return a list for FILENAME '(<needs-more> <preview-string>)'.
The former is either t or nil. indicating whether a \"Read
More...\" link is needed. The text is either extracted from an
org-mode comment block or from the first paragraph of text."
  (with-temp-buffer
    (insert-file-contents (concat "./posts/" filename))
    (goto-char (point-min))
    (let ((marker-start (or
                          ;; Look for the first non-tag line
                          (and (re-search-forward "^#\\+begin_comment$" nil t)
                               (match-end 0))
                          ;; Failing that, assume we have no preview
                          (buffer-size)))
          (marker-end (or
                       (and (re-search-forward "^#\\+end_comment$" nil t)
                            (match-beginning 0))
                       (buffer-size))))
      (when (= marker-start (buffer-size))
          ;; place the marker-start at the first line of content instead
          (goto-char (point-min))
          (setq marker-start (or
                              ;; Look for the first non-tag line
                              (and (re-search-forward "^[^#]" nil t)
                                   (match-beginning 0))
                              ;; Failing that, assume we're malformed (and )
                              ;; have no content
                              (buffer-size))
                ;; if no preview is encapsulated in a block we use first paragraph only
                marker-end (progn (forward-paragraph) (point)))
          )
      ;; Return a pair of '(needs-more preview-string)
      (list (not (= marker-end (buffer-size)))
            (string-trim (buffer-substring marker-start marker-end))))))



(defun hanno/org-sitemap-format-tag (entry style project)
  "Format tag with author and published data for index or tag pages.

ENTRY: file-name
STYLE:
PROJECT: `posts` in this case."
  (format "*[[file:%s][%s]]*"
          ;; file
          entry
          ;; title
          (org-publish-find-title entry project)))

(defun hanno/org-format-entry (entry project)
  "Format posts with author and published data for index or tag pages.

ENTRY: file-name
PROJECT: `posts` in this case."
  (let* (
         ;; retrieve the org-mode #+FILETAGS property from `file' as list of strings
         (tags (hanno/string-to-list (+org-get-global-property "filetags" (concat (file-name-as-directory (or (plist-get (cdr project) :base-directory) "tags")) entry))))
         (kwdir (plist-get (cdr project) :tag-directory))
         ;; author and date
         (pubdate (format "#+HTML: <p class='pubdate'>by %s on %s.</p>"
                          (car (org-publish-find-property entry :author project))
                          (format-time-string psachin-date-format (org-publish-find-date entry project))))
         (title (format "%s" (org-publish-find-title entry project)))
         (description
          (with-temp-buffer
            (insert (format "[[file:%s][%s]]\n%s\n%s\n\nTags: %s"
                             ;; filename
                             entry
                             title
                             pubdate
                             ;; shortened text
                             (let* ((preview (loomcom/get-preview entry))
                                    (needs-more (car preview))
                                    (preview-text (cadr preview)))
                               (if needs-more
                                   (format
                                    "%s [[file:%s][Read More...]]"
                                    preview-text entry)
                                 (format "%s" preview-text)
                                 ))
                             ;; tags
                             (let (kwlinks)
                               (dolist (tag tags (mapconcat 'identity (nreverse kwlinks) ", " ))
                                 (push (format "[[file:%s][%s]]"
                                               (concat (file-name-as-directory kwdir)
                                                       tag ".org") tag) kwlinks)))))
            ;; double-blank lines cause EOF in index
            (hanno/remove-subsequent-blank-lines)
            (buffer-string))))
    ;; now loop over tags and create index files for each one
    (dolist (tag tags)
      (let ((tag.org (expand-file-name (format "%s.org" tag) kwdir)))
        (unless (file-exists-p tag.org)
          (with-temp-file tag.org
            (insert (format
                     "#+TITLE: /%s/\n#+INCLUDE: \"../org-templates/tag.inc\"
          [[file:index.org][◉ List of all tags]]
          [[file:../index.org][◉ Back to chronological index]]\n" tag))))
        (with-temp-buffer
          (insert (format "- *[[file:%s][%s]]*\n\
             %s\n" (concat "../" entry) title pubdate ))
          (append-to-file (point-min) (point-max) tag.org))))
    description))

(defun psachin/org-sitemap-format-entry (entry style project)
  "Format posts with author and published data in the index page.

ENTRY: file-name
STYLE:
PROJECT: `posts` in this case."
  (cond ((not (directory-name-p entry))
         (hanno/org-format-entry entry project))
        ((eq style 'tree) (file-name-nondirectory (directory-file-name entry)))
        (t entry)))

(defun hanno/org-clear-tags-directory (project)
  "Remove all entries in the `tags' directory specified by the :tag-directory property and make sure the directory exists for the PROJECT."

  (let ((kwdir (or (plist-get (cdr project) :base-directory) "tags")))
    (unless (file-directory-p kwdir)
      (make-directory kwdir))
    (dolist (file (directory-files-recursively kwdir ".*\.org"))
      (delete-file file))))

(defun psachin/org-reveal-publish-to-html (plist filename pub-dir)
  "Publish an org file to reveal.js HTML Presentation.
FILENAME is the filename of the Org file to be published.  PLIST
is the property list for the given project.  PUB-DIR is the
publishing directory. Returns output file name."
  (let ((org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/"))
    (org-publish-org-to 'reveal filename ".html" plist pub-dir)))

(defun my-tag-injection (_backend)
  "Add tags to the end of org buffers."
  (let* (
         (case-fold-search t)
         (tags (hanno/string-to-list (+org-get-global-property "filetags"))))
    (save-excursion
      (goto-char (point-max))
      (when (and tags (not (string-match-p "about" (format "%s" tags))))
        (insert (format "\n\n Tags: %s"
                        (let (kwlinks)
                          (dolist (tag tags (mapconcat 'identity (nreverse kwlinks) ", " ))
                            (push (format "[[file:%s][%s]]"
                                          (concat "../"
                                                  (file-name-as-directory hanno/tag-directory)
                                                  tag ".org") tag) kwlinks)))))))))

(add-hook 'org-export-before-parsing-hook 'my-tag-injection)

;; RSS stuff adapted from from https://writepermission.com/org-blogging-rss-feed.html

;; do not prettify the xml output of the RSS export (saves time)
(defun org-rss-final-function (contents backend info) contents)

(defun rw/org-rss-publish-to-rss (plist filename pub-dir)
  "Publish RSS with PLIST, only when FILENAME is 'rss.org'.
PUB-DIR is when the output will be placed."
  (if (equal "rss.org" (file-name-nondirectory filename))
      (org-rss-publish-to-rss plist filename pub-dir)))

(defun rw/format-rss-feed (title list)
  "Generate RSS feed, as a string.
TITLE is the title of the RSS feed.  LIST is an internal
representation for the files to include, as returned by
`org-list-to-lisp'.  PROJECT is the current project."
  (concat "#+TITLE: " title "\n#+SETUPFILE: ../org-templates/post.org\n\n"
          (org-list-to-subtree list 1 '(:icount "" :istart ""))))

(defun rw/format-rss-feed-entry (entry style project)
  "Format ENTRY for the RSS feed.
ENTRY is a file name.  STYLE is either 'list' or 'tree'.
PROJECT is the current project."
  (cond ((not (directory-name-p entry))
         (let* ((file (org-publish--expand-file-name entry project))
                (title (org-publish-find-title entry project))
                (date (format-time-string "%Y-%m-%d" (org-publish-find-date entry project)))
                (link (concat (file-name-sans-extension entry) ".html")))
           (with-temp-buffer
             (org-mode)
             (insert (format "* %s\n" title))
             (org-set-property "RSS_PERMALINK" link)
             (org-set-property "PUBDATE" date)
             ;; insert post content w/o header info
             (hanno/insert-org-file-without-header file)
             ;; remove double-blank lines as they seem to break
             ;; something in the file generation (cause EOF)
             (hanno/remove-subsequent-blank-lines)
             (buffer-string))))
        ((eq style 'tree)
         ;; Return only last subdir.
         (file-name-nondirectory (directory-file-name entry)))
        (t entry)))

;;
;; Helper functions
;;

(defun hanno/insert-org-file-without-header(file)
  "Insert content of FILE without the file property header into current buffer."
  (insert
   (with-temp-buffer
     (insert-file-contents file)
     (string-trim (buffer-substring
                   ;; start marker
                   (or
                    ;; Look for the first non-tag line
                    (and (re-search-forward "^[^#:]" nil t)
                         (match-beginning 0))
                    ;; have no content
                    (buffer-size))
                   ;; end marker
                   (buffer-size))
                  ))))


(defun hanno/remove-subsequent-blank-lines ()
  "Remove subsequent blank lines from the current buffer."
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "^\n\\{2,\\}" nil t)
      (replace-match "\n" nil nil))))

(defun hanno/string-to-list (astring)
  "Convert string (as ASTRING) into a list."

  ;; remove empty entries
  (delete ""
          ;; split string into list
          (ignore-errors (split-string
                          ;; remove commas
                          (replace-regexp-in-string (regexp-quote ",")
                                                    "" astring nil 'literal)))))


;;
;; Project configuration
;;

(setq org-publish-project-alist
      `(
        ("posts"
         :base-directory "posts"
         :base-extension "org"
         :recursive t
         :preparation-function hanno/org-clear-tags-directory
         :publishing-function org-html-publish-to-html
         :publishing-directory "./public"
         :exclude ,(regexp-opt '("README.org" "draft" "rss.org" "404.org"))
         :auto-sitemap t
         :sitemap-filename "index.org"
         :sitemap-title "hoowl:~$  ls posts/"
         :sitemap-format-entry psachin/org-sitemap-format-entry
         :sitemap-style list
         :sitemap-function hanno/org-publish-org-sitemap
         :sitemap-sort-files anti-chronologically
         :tag-directory ,hanno/tag-directory
         :html-link-home ""
         :html-link-up ""
         :html-head-include-scripts t
         :html-head-include-default-style nil
         :html-head ,psachin-website-html-head
         :html-preamble psachin-website-html-preamble
         :html-postamble ,psachin-website-html-postamble)
        ("tags"
         :base-directory "tags"
         :base-extension "org"
         :publishing-function org-html-publish-to-html
         :publishing-directory "./public/tags"
         :exclude ,(regexp-opt '("README.org" "draft" "rss.org" "404.org"))
         :auto-sitemap t
         :sitemap-filename "index.org"
         :sitemap-title "hoowl:~$ ls tags/"
         :sitemap-format-entry hanno/org-sitemap-format-tag
         :sitemap-style list
         :sitemap-sort-files alphabetically
         :html-link-home "/"
         :html-link-up "/"
         :html-head-include-scripts t
         :html-head-include-default-style nil
         :html-head ,psachin-website-html-head
         :html-preamble psachin-website-html-preamble
         :html-postamble ,psachin-website-html-postamble)
        ("about"
         :base-directory "about"
         :base-extension "org"
         :exclude ,(regexp-opt '("README.org" "draft" "rss.org" "404.org"))
         :index-filename "index.org"
         :recursive nil
         :publishing-function org-html-publish-to-html
         :publishing-directory "./public/about"
         :html-link-home "/"
         :html-link-up "/"
         :html-head-include-scripts t
         :html-head-include-default-style nil
         :html-head ,psachin-website-html-head
         :html-preamble psachin-website-html-preamble
         :html-postamble ,psachin-website-html-postamble)
        ("css"
         :base-directory "./css"
         :base-extension "css"
         :publishing-directory "./public/css"
         :publishing-function org-publish-attachment
         :recursive t)
        ("fonts"
         :base-directory "./fonts"
         :base-extension ,site-attachments
         :publishing-directory "./public/fonts"
         :publishing-function org-publish-attachment
         :recursive t)
        ("images"
         :base-directory "./images"
         :base-extension ,site-attachments
         :publishing-directory "./public/images"
         :publishing-function org-publish-attachment
         :recursive t)
        ("assets"
         :base-directory "./assets"
         :base-extension ,site-attachments
         :publishing-directory "./public/assets"
         :publishing-function org-publish-attachment
         :recursive t)
        ("rss"
         :base-directory "posts"
         :base-extension "org"
         :recursive nil
         :exclude ,(regexp-opt '("draft" "rss.org" "index.org" "404.org"))
         :publishing-function rw/org-rss-publish-to-rss
         :rss-image-url "http://www.hoowl.se/images/android-chrome-256x256.png"
         :publishing-directory "./public"
         :rss-extension "xml"
         :html-link-home "http://www.hoowl.se"
         :html-link-use-abs-url t
         :html-link-org-files-as-html t
         :auto-sitemap t
         :sitemap-filename "rss.org"
         :sitemap-title "hoowl"
         :sitemap-style list
         :sitemap-sort-files anti-chronologically
         :sitemap-function rw/format-rss-feed
         :sitemap-format-entry rw/format-rss-feed-entry)
        ("all" :components ("posts" "tags" "about" "css" "images" "assets" "rss"))))

(provide 'publish)
;;; publish.el ends here
